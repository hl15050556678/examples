BITS 64
SECTION .text

; define main as globale, which is recognizable by the linker  
global main

main:
	jmp short data
code:
	; replace end marker `0` by 0
	pop rsi
	xor rax, rax
	mov BYTE [rsi + 17], al

	; print message
	xor rdx, rdx
	mov dl, 1    ; file descriptor
	mov rdi, rdx
	mov dl, 17 ; message length
	xor rax, rax
	mov al, 1 ; syscall number for write
	syscall
	
	; exit application
	xor rdi, rdi ; error number
	xor rax, rax
	mov al, 60 ; syscall number for exit
	syscall
data:
	call code
	message db "Hello students!", 10, 13, `0`
ALIGN 64
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	; override return address with 401014
	DD 0x401014
